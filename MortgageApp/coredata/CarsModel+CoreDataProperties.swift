//
//  CarsModel+CoreDataProperties.swift
//  CarsApp
//
//  Created by Rydus on 14/08/2019.
//  Copyright © 2019 7PeaksSoftware. All rights reserved.
//
//

import Foundation
import CoreData


extension CarsModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CarsModel> {
        return NSFetchRequest<CarsModel>(entityName: "CarsModel")
    }

    @NSManaged public var json: NSObject?

}
