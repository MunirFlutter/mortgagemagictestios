//
//  UserModel+CoreDataProperties.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//
//

import Foundation
import CoreData


extension UserModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserModel> {
        return NSFetchRequest<UserModel>(entityName: "UserModel")
    }

    @NSManaged public var json: Data?

}
