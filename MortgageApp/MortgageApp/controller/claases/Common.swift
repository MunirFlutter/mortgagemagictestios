//
//  Common.swift
//  CarsApp
//
//  Created by Rydus on 14/08/2018.
//  Copyright © 2018 7PeaksSoftware. All rights reserved.
//

import Foundation
final class Common {
    static func Log(str:String) {
        debugPrint("log::%@", str);
    }

    static func isEqual<T:Equatable>(_ arg1:T, _ arg2:T)->Bool {
        return arg1 == arg2
    }
    
    static func randomBool() -> Bool {
        return arc4random_uniform(2) == 0
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
