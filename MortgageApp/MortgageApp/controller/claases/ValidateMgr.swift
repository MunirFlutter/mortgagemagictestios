//
//  ValidateMgr.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import Foundation
class ValidateMgr {    
    static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
