//
//  NetworkMgr.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import Foundation
import APESuperHUD

class NetworkMgr:NSObject{
    
    static func post(parameters:[String:Any], action_url:String, isCookie:Bool, completion: @escaping (_ result: Data)->()) {
        let url = Server.BASE_URL+action_url
        guard let uri = URL(string: url) else { return }
        if NetworkState.isConnected() {
           DispatchQueue.main.async() {
                APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Please wait...")
           }
           let request = NSMutableURLRequest(url: uri)
           //  uncomment this and add auth token, if your project needs.
           //  let config = URLSessionConfiguration.default
           //  let authString = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxMywiUGFzc3dvcmQiOiIkMmEkMTAkYVhpVm9wU3JSLjBPYmdMMUk2RU5zdU9LQzlFR0ZqNzEzay5ta1pDcENpMTI3MG1VLzR3SUsiLCJpYXQiOjE1MTczOTc5MjV9.JaSh3FvpAxFxbq8z_aZ_4OhrWO-ytBQNu6A-Fw4pZBY"
           //  config.httpAdditionalHeaders = ["Authorization" : authString]
           let session = URLSession.shared
           request.httpMethod = "POST"
           request.httpShouldHandleCookies = isCookie
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           request.addValue("application/json", forHTTPHeaderField: "Accept")
           request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
            if(isCookie) {
                
                let config = URLSessionConfiguration.default
                let session = URLSession(configuration: config, delegate: nil, delegateQueue: nil)
                session.configuration.httpCookieAcceptPolicy = .always
                session.configuration.httpCookieStorage = HTTPCookieStorage.shared
                session.configuration.httpShouldSetCookies = true
                
                let jar = HTTPCookieStorage.shared
                let cookieHeaderField = ["Set-Cookie": parameters.queryString] // Or ["Set-Cookie": "key=value, key2=value2"] for multiple cookies
                let cookies = HTTPCookie.cookies(withResponseHeaderFields: cookieHeaderField, for: uri)
                jar.setCookies(cookies, for: uri, mainDocumentURL: uri)
           }            
           session.dataTask(with: request as URLRequest) { data, response, error in
               DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    APESuperHUD.dismissAll(animated: true)
               })
               guard let data = data else { return }
               completion(data)
            }.resume()
        }
   }
    
   private static func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        let httpResponse = response as? HTTPURLResponse
        //var dict = httpResponse?.allHeaderFields
        let cookies = HTTPCookie.cookies(withResponseHeaderFields: httpResponse?.allHeaderFields as! [String : String], for: response.url!)
        dump(cookies) // for testing.
        //
        // some process to get/save cookie.
        //
    }
    
    static func get(action_url:String, completion: @escaping (_ result: Data)->()) {
           let url = Server.BASE_URL+action_url
           guard let uri = URL(string: url) else { return }
           if NetworkState.isConnected() {
              DispatchQueue.main.async() {
                   APESuperHUD.show(style: .loadingIndicator(type: .standard), title: nil, message: "Please wait...")
              }
              let request = NSMutableURLRequest(url: uri)
              let session = URLSession.shared
              request.httpMethod = "GET"
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              request.addValue("application/json", forHTTPHeaderField: "Accept")
              session.dataTask(with: request as URLRequest) { data, response, error in
                  DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                       APESuperHUD.dismissAll(animated: true)
                  })
                  guard let data = data else { return }
                  completion(data)
               }.resume()
           }
      }
}

extension Dictionary {
    var queryString: String {
        var output: String = ""
        for (key,value) in self {
            output +=  "\(key)=\(value),"
        }
        output = String(output.dropLast())
        return output
    }
}
