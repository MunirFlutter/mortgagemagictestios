//
//  NetworkState.swift
//  CarsApp
//
//  Created by Rydus on 14/08/2019.
//  Copyright © 2019 7PeaksSoftware. All rights reserved.
//

import Foundation
import Alamofire

class NetworkState {
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
