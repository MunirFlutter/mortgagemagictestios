//
//  Login.swift
//  CarWash
//
//  Created by Rydus on 18/09/2019.
//  Copyright © 2019 Rydus. All rights reserved.
//

import Foundation
import SwiftyJSON

struct modelTaskList: Decodable {
    var Success: Bool!
    let ErrorMessages:JSON?
    let Messages:JSON?
    let ResponseData:JSON?
}
