//
//  BaseVC.swift
//  MortgageApp
//
//  Created by rydus on 2/6/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {

    var screenSize = CGRect()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !NetworkState.isConnected() {
            showAlert(msg: "Internet is not available")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        screenSize = UIScreen.main.bounds
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        upgradeStatusBar()
    }

    //  MARK:   -   MsgBox
    func showAlert(msg:String) {
        let alert = UIAlertController(title:nil, message:msg, preferredStyle:.alert)
        alert.view.backgroundColor = .black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        self.navigationController?.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3.0) {
            alert.dismiss(animated:true)
        }
    }
    
    //  MARK:   set statusbar color
    func upgradeStatusBar() {
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
            statusBar.backgroundColor = .systemBlue
            UIApplication.shared.keyWindow?.addSubview(statusBar)
        }
    }
    
    func upgradeViewShade(_ view:UIView) {
        view.layer.borderWidth = 0.5;
        view.layer.borderColor = UIColor.darkGray.cgColor;
        view.layer.cornerRadius = 10;
        view.layer.masksToBounds = true;
    }
    
    //  MARK    -   Cache json into db = replaement of cookie!
    func setUserProfileDB(data:Data) {
        //  Background GCD Thread for storing data into db as cache
        DispatchQueue.global(qos: .background).async {
            //  saving into db as cache json
            DBMgr.shared.delRows(entity: "UserModel")   //  remove old and restore new json
            let db = UserModel(context: DBMgr.shared.context)
            db.json = data
            DBMgr.shared.save()
        }
    }
}

//  MARK:   set prefix or suffix icon with button title
extension UIButton {
    func setAttributedTextWithImagePrefix(image: UIImage, text: String, color:UIColor, for state: UIControl.State) {
        let fullString = NSMutableAttributedString()

        if let imageString = getImageAttributedString(image: image) {
            fullString.append(imageString)
        }

        fullString.append(NSAttributedString(string: "  " + text, attributes: [NSAttributedString.Key.foregroundColor : color]))

        self.setAttributedTitle(fullString, for: state)
    }

    func setAttributedTextWithImageSuffix(image: UIImage, text: String, color:UIColor, for state: UIControl.State) {
        let fullString = NSMutableAttributedString(string: text + "  ", attributes: [NSAttributedString.Key.foregroundColor : color])

        if let imageString = getImageAttributedString(image: image) {
            fullString.append(imageString)
        }

        self.setAttributedTitle(fullString, for: state)
    }

    fileprivate func getImageAttributedString(image: UIImage) -> NSAttributedString? {
        let buttonHeight = self.frame.height

        if let resizedImage = image.getResizedWithAspect(maxHeight: buttonHeight - 10) {
            let imageAttachment = NSTextAttachment()
            imageAttachment.bounds = CGRect(x: 0, y: ((self.titleLabel?.font.capHeight)! - resizedImage.size.height).rounded() / 2, width: resizedImage.size.width, height: resizedImage.size.height)
            imageAttachment.image = resizedImage
            let image1String = NSAttributedString(attachment: imageAttachment)
            return image1String
        }

        return nil
    }
}


extension UIImage {
    func getResized(size: CGSize) -> UIImage? {
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale);
        } else {
            UIGraphicsBeginImageContext(size);
        }

        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height));
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        return newImage;
    }

    func getResizedWithAspect(scaledToMaxWidth width: CGFloat? = nil, maxHeight height: CGFloat? = nil) -> UIImage? {
        let oldWidth = self.size.width;
        let oldHeight = self.size.height;

        var scaleToWidth = oldWidth
        if let width = width {
            scaleToWidth = width
        }

        var scaleToHeight = oldHeight
        if let height = height {
            scaleToHeight = height
        }

        let scaleFactor = (oldWidth > oldHeight) ? scaleToWidth / oldWidth : scaleToHeight / oldHeight;

        let newHeight = oldHeight * scaleFactor;
        let newWidth = oldWidth * scaleFactor;
        let newSize = CGSize(width: newWidth, height: newHeight);

        return getResized(size: newSize);
    }
}

//  MARK:   shadow
extension UIView {
    func addShadowView(width:CGFloat=0.2, height:CGFloat=0.2, Opacidade:Float=1, maskToBounds:Bool=false, radius:CGFloat=1){      
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:2)
    }
}

//  MARK:   making scrollview contentsize scrollable in constraint
func setupScrollviewConstraints(view:UIView, scrollView:UIScrollView) {
    // Constraints for scrollView
    scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
}

