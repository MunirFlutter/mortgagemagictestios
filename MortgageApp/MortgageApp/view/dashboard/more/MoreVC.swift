//
//  MoreVC.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit

class MoreVC: BaseVC {

    @IBOutlet weak var tableView:UITableView!
    let list = [
        "Profile",
        "Reviews",
        "Notifications",
        "Settings",
        "Help",
        "Logout",
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = screenSize.height*0.1
    }
}

extension MoreVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "cell")
        cell.textLabel?.text = list[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.isUserInteractionEnabled = true
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt
    indexPath: IndexPath){
        print("You selected cell #\(list[indexPath.row])!")
        switch indexPath.row {
        case 0:
            print("push profile")
        case 1:
            print("push review")
        case 2:
            print("push notification")
        case 3:
            print("push eetting")
        case 4:
            print("push help")
        case 5:
            print("push logout")
            DBMgr.shared.delRows(entity: "UserModel")
            /*let navigationController = UINavigationController(navigationBarClass: NMain.self, toolbarClass: UIToolbar.self)

            navigationController.setViewControllers([yourRootViewController], animated: false)*/
            let rootVC = self.storyboard?.instantiateViewController(identifier: "NMain") as? NMain
            UIApplication.shared.windows.first?.rootViewController =  rootVC
        default:
            print("nothing")
        }
    }
}
