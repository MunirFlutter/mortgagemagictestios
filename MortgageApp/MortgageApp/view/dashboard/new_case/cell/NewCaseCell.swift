//
//  NewCaseCell.swift
//  MortgageApp
//
//  Created by rydus on 2/9/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit

class NewCaseCell: UICollectionViewCell {
    @IBOutlet weak var image:UIImageView!
    @IBOutlet weak var text:UILabel!
}
