//
//  PostNewCaseVC.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit

class PostNewCaseVC: BaseVC {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var caseType:UITextField!
    @IBOutlet weak var caseDesc:UITextView!
    @IBOutlet weak var sw:UISwitch!
    @IBOutlet weak var btnContinue:UIButton!
    
    var dicTask = Dictionary<String, Any>()
    
    func passData(_ dic: Dictionary<String, Any>) {
        self.dicTask = dic
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upgradeUI()
    }
    
    func upgradeUI() {
        
        self.title = "New Case"
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        navigationItem.largeTitleDisplayMode = .never
        
        setupScrollviewConstraints(view: self.view, scrollView: scrollView)
        
        caseType.delegate = self
        caseDesc.delegate = self
        caseDesc.layer.borderWidth = 1.0
        
        //  MARK:   curve login button
        btnContinue.layer.cornerRadius = 15;
        btnContinue.layer.masksToBounds = true;
    }

    @IBAction func onSWChanged(_ sender : UISwitch!){
       if sender.isOn {
           //companyNameView.isHidden =  false
       } else {
          //companyNameView.isHidden =  true
       }
    }
       
    @IBAction func continueClicked(_ sender: UIButton) {
        
        var user_id = dicTask["UserId"]
        //   *************************   hard code UserId for showing rows
        user_id = 117533
        var isAnyOther = "No"
        if(sw.isOn) {
            isAnyOther = "Yes"
        }
        
        let mortgageInfoArr = dicTask["MortgageCaseInfoEntityModelList"] as! Array<Any>
        let mortgageInfoDic = mortgageInfoArr[0] as! [String : Any]
        let subParam = [
            "UserId":user_id!,
            "CompanyId":dicTask["CompanyId"]!,
            "TaskId":0,
            "Status":0,
            "CreationDate":"17-Oct-2020",
            "UpdatedDate":"17-Oct-2020",
            "VersionNumber":0,
            "CaseType":caseType.text!,
            "CustomerType":"",
            "IsSmoker":"No",
            "Remarks":"",
            "IsAnyOthers":isAnyOther,
            "IsCurrentProperty":"No",
            "CustomerName":mortgageInfoDic["CustomerName"] as! String,
            "CustomerEmail":mortgageInfoDic["CustomerEmail"] as! String,
            "CustomerMobileNumber":mortgageInfoDic["CustomerMobileNumber"] as! String,
            "CustomerAddress":"",
            "CoapplicantUserId":0,
            "AreYouBuyingThePropertyInNameOfASPV":"No",
            "CompanyName":dicTask["CompanyName"] as! String,
            "RegisteredAddress":"",
            "DateRegistered":"01-Jan-1970",
            "CompanyRegistrationNumber":"",
            "DateRegistered1":"",
            "DateRegistered2":"",
            "DateRegistered3":"",
            "AdminFee":0,
            "AdminFeeWhenPayable":"",
            "AdviceFee":0,
            "AdviceFeeWhenPayable":"",
            "IsFeesRefundable":"",
            "FeesRefundable":""
        ] as [String : Any]
        
        //let subParamArr = [subParam]
        
        let param = [
                "Status":903,
                "Title":dicTask["Title"] as! String,
                "Description":caseDesc.text!,
                "IsInPersonOrOnline":false,
                "DutDateType":0,
                "DeliveryDate":"17-Oct-2020",
                "DeliveryTime":"",
                "WorkerNumber":1,
                "Skill":"",
                "IsFixedPrice":true,
                "HourlyRate":0,
                "FixedBudgetAmount":0,
                "NetTotalAmount":0,
                "JobCategory":dicTask["JobCategory"] as! String,
                "PreferedLocation":dicTask["PreferedLocation"] as! String,
                "Requirements":"",
                "TotalHours":0,
                "Latitude":0,
                "Longitude":0,
                "TaskReferenceNumber":"",
                "ReferenceTaskerId":0,
                "MortgageCaseInfoEntityModelList":[],
                "UserId":user_id!,
                "EntityId":dicTask["EntityId"]!,
                "EntityName":"",
                "CompanyId":dicTask["CompanyId"]!,
                "MortgageCaseInfoEntity":subParam,
            ] as [String : Any]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: param, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        NetworkMgr.post(parameters: param, action_url:  Server.POSTCASE_URL, isCookie:false) { (dataResponse) in
                do {
                    guard let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? NSDictionary else {
                        return
                    }
                    print(json.description)
                    //  MARK:   Dependency Injection:: injecting json into model
                    //let model = try! JSONDecoder().decode(modelPostCase.self, from: dataResponse)
                    //  Temporary fixed bcz having error while posting
                    DispatchQueue.main.async {
                        self.showAlert(msg: json["message"] as! String)
                    }
                    /*if(!model.Success) {
                        let dic = model.ErrorMessages?.dictionaryObject
                        let arrayValues = dic?["task_post"] as! Array<Any>
                        DispatchQueue.main.async {
                            self.showAlert(msg: arrayValues[0] as! String)
                        }
                        return
                    }
                    else {
                        
                    }*/
                }catch {
                    Common.Log(str: error.localizedDescription)
                }
            }
        }
}

extension PostNewCaseVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Task Note"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension PostNewCaseVC:UITextFieldDelegate {
    
    private func textFieldDidBeginEditing(textField: UITextField!) {
        
    }

    private func textFieldShouldEndEditing(textField: UITextField!) -> Bool {  //delegate method
        return false
    }

    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}
