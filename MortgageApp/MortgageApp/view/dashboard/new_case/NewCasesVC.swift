//
//  NewCasesVC.swift
//  MortgageApp
//
//  Created by rydus on 2/8/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit

class NewCasesVC: BaseVC {

    @IBOutlet weak var collectionView:UICollectionView!
    
    let caseIconsArray = [
        "ic_case_1",
        "ic_case_2",
        "ic_case_3",
        "ic_case_4",
        "ic_case_5",
        "ic_case_6",
        "ic_case_7",
        "ic_case_8",
        "ic_case_9",
    ]
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 10,
        minimumLineSpacing: 10,
        sectionInset: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    )
    
    var locationsList = Array<Any>()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchTaskList()
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        upgradeUI()
    }

    func upgradeUI() {
        collectionView?.collectionViewLayout = columnLayout
        collectionView?.contentInsetAdjustmentBehavior = .always
        //collectionView.register(NewCaseCell.self, forCellWithReuseIdentifier: "cell")

    }
    
    func fetchTaskList() {
        DBMgr.shared.getUserJson { (userModel) in
            if let dic = Common.convertToDictionary(text: String(data: userModel.json!, encoding: .utf8)!) {
                print(dic.description as Any)
                
               let dic2 = dic["UserCompanyInfo"] as! Dictionary<String, Any>
               var user_id = String(format: "%i",dic2["UserId"] as! CVarArg)
               //   *************************   hard code UserId for showing rows
               user_id = "115767"
               let url = Server.TASKLIST_URL.replacingOccurrences(of: "#USER_ID#", with: user_id, options: .literal, range: nil);
               print(url)
               NetworkMgr.get(action_url: url) { (dataResponse) in
                   do {
                       guard let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? NSDictionary else {
                           return
                       }
                       print(json.description)
                       //  MARK:   Dependency Injection:: injecting json into model
                       let model = try! JSONDecoder().decode(modelTaskList.self, from: dataResponse)
                       if(!model.Success) {
                           //let dic = model.ErrorMessages?.dictionaryObject
                           //let arrayValues = dic?["task"] as! Array<Any>
                           DispatchQueue.main.async {
                               //self.showAlert(msg: arrayValues[0] as! String)
                            self.showAlert(msg: "Sorry, tast information not found")
                           }
                           return
                       }
                       else {
                           //  MARK:   on login success
                           let dic = model.ResponseData?.dictionaryObject
                           self.locationsList = dic!["Locations"] as! Array<Any>
                           print(self.locationsList.description)
                           //   render collection grid
                           DispatchQueue.main.async {
                                self.collectionView.reloadData()
                           }
                       }
                   }catch {
                       Common.Log(str: error.localizedDescription)
                   }
               }
           }
            
        }
        
    }
    
    /*@IBAction func postCaseClicked() {
        self.navigationController!.pushViewController(storyboard!.instantiateViewController(withIdentifier: "PostNewCaseVC") as! PostNewCaseVC, animated: true)
    }*/

}

extension NewCasesVC:UICollectionViewDelegate, UICollectionViewDataSource {
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.locationsList.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! NewCaseCell
        
        let dic = self.locationsList[indexPath.row] as! Dictionary<String, Any>
        cell.text.text = dic["Title"] as? String
        //let imgCaseIndex = dic[""] as? Int
        let imgCaseIndex = Int(arc4random_uniform(UInt32(caseIconsArray.count)))
        if(imgCaseIndex < caseIconsArray.count) {
            cell.image.image = UIImage.init(named: caseIconsArray[imgCaseIndex])
        }
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        let vc = storyboard!.instantiateViewController(withIdentifier: "PostNewCaseVC") as! PostNewCaseVC
        vc.passData(self.locationsList[indexPath.row] as! Dictionary<String, Any>)
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    
}
