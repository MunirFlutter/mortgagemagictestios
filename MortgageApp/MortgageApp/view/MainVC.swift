//
//  ViewController.swift
//  MortgageApp
//
//  Created by rydus on 2/6/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit
import QuartzCore

class MainVC: BaseVC {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var btnStart:UIButton!
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden =  false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden =  true
    }
    
    override func loadView() {
        if(DBMgr.shared.getNumRows(entity: "UserModel")>0) {
            UIApplication.shared.windows.first?.rootViewController = storyboard!.instantiateViewController(withIdentifier: "TabDashboard") as! UITabBarController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        super.loadView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //  MARK:   if user is already registered then switch into the dashboard
        if(DBMgr.shared.getNumRows(entity: "UserModel")==0) {
            upgradeUI()
        }
    }

    //  MARK:   add arrow icon in button
    func upgradeUI() {
        
        setupScrollviewConstraints(view: self.view, scrollView: scrollView)
        
        btnStart.layer.cornerRadius = 10
        btnStart.layer.borderWidth = 1
        btnStart.setImage(#imageLiteral(resourceName: "ic_play_arrow_black_24dp"), for: .normal)
        btnStart.semanticContentAttribute = .forceRightToLeft
        btnStart.imageView?.tintColor = .black
        let buttonWidth = btnStart.frame.width
        let spacing: CGFloat = 8.0 / 2
        btnStart.imageEdgeInsets = UIEdgeInsets(top: 0, left: buttonWidth/3, bottom: 0, right: -spacing)
    }
    
    @IBAction func startClicked(_ sender: UIButton) {
        self.navigationController!.pushViewController(storyboard!.instantiateViewController(withIdentifier: "AuthVC") as! AuthVC, animated: true)
    }
}


