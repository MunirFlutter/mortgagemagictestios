//
//  LoginVC.swift
//  MortgageApp
//
//  Created by rydus on 2/6/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginVC: BaseVC {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var tf1View:UIView!
    @IBOutlet weak var tf2View:UIView!
    @IBOutlet weak var email:SkyFloatingLabelTextField!
    @IBOutlet weak var pwd:SkyFloatingLabelTextField!
    @IBOutlet weak var btnLogin:UIButton!
    @IBOutlet weak var btnFBLogin:UIButton!
    @IBOutlet weak var btnGLogin:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upgradeUI()
    }
    
    func upgradeUI(){
        
         setupScrollviewConstraints(view: self.view, scrollView: scrollView)
        
        //  MARK:   TF delegate
        email.text = "anisur5001@yopmail.com"   //  tmp
        pwd.text = "123456" //  tmp
        email.delegate = self
        pwd.delegate = self
        email.lineColor = .clear
        pwd.lineColor = .clear
        
        //  MARK:   view shade
        upgradeViewShade(tf1View)
        upgradeViewShade(tf2View)
        
        //  MARK:   curve login button
        btnLogin.layer.cornerRadius = 15;
        btnLogin.layer.masksToBounds = true;
        
        //  MARK:   set fb icon
        btnFBLogin.setAttributedTextWithImagePrefix(image: #imageLiteral(resourceName: "fb_icon"), text: "Continue with Facebook", color: .white, for: .normal)

        //  MARK:   set google icon
        btnGLogin.setAttributedTextWithImagePrefix(image: #imageLiteral(resourceName: "g_icon"), text: "Sign in", color: .black, for: .normal)
        btnGLogin.backgroundColor = .white
        btnGLogin.addShadowView()
        
    }
    
    @IBAction func forgotClicked(_ sender: UIButton) {
        //  MARK:   Twilio or other 3rd party api to send code or by Firebase Messaging
    }
    
    @IBAction func loginClicked(_ sender: UIButton) {
        //  MARK:   JSon API call
        if(email.text?.count==0) {
            showAlert(msg: "Please enter valid email")
            return
        }
        else if(pwd.text?.count==0) {
            showAlert(msg: "Please enter password")
            return
        }
        else {            
            let param = [
                "Email": email.text!,
                "Password": pwd.text!,
                "Persist":false,
                "CheckSignUpMobileNumber":false,
                "CountryCode":"880",
                "Status":"101",
                "OTPCode":"",
                "BirthDay":"",
                "BirthMonth":"",
                "BirthYear":"",
                "UserCompanyId":"0"
            ] as [String : Any]
        NetworkMgr.post(parameters: param, action_url:  Server.LOGIN_URL, isCookie:true) { (dataResponse) in
                do {
                    guard let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? NSDictionary else {
                        return
                    }
                    print(json.description)
                    //  MARK:   Dependency Injection:: injecting json into model
                    let model = try! JSONDecoder().decode(modelLogin.self, from: dataResponse)
                    if(!model.Success) {
                        let dic = model.ErrorMessages?.dictionaryObject
                        let arrayValues = dic?["login"] as! Array<Any>
                        DispatchQueue.main.async {
                            self.showAlert(msg: arrayValues[0] as! String)
                        }
                        return
                    }
                    else {
                        //  MARK:   on login success
                        let dic = model.ResponseData?.dictionaryObject
                        let dic2 = dic!["User"] as! Dictionary<String, Any>
                        let jsonData = try! JSONSerialization.data(withJSONObject: dic2, options: [])
                        self.setUserProfileDB(data: jsonData)
                        //  let decoded = String(data: jsonData!, encoding: .utf8)!
                        DispatchQueue.main.async {
                            UIApplication.shared.windows.first?.rootViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabDashboard") as! UITabBarController
                                       UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }                    
                }catch {
                    Common.Log(str: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func loginMobClicked(_ sender: UIButton) {
           //  MARK:   Firebase Mobile Auth Code enable
         self.showAlert(msg: "Need firebase phone auth implementation")
    }
    
    @IBAction func loginFBClicked(_ sender: UIButton) {
           //   MARK:   Firebase Facebook enable
        self.showAlert(msg: "Need firebase facebook id, secret code auth implementation")
    }
    
    @IBAction func loginGClicked(_ sender: UIButton) {
            //  MARK:   Firebase Google GMail enable
        self.showAlert(msg: "Need firebase google gmail auth implementation")
    }
}

extension LoginVC:UITextFieldDelegate {
    
    private func textFieldDidBeginEditing(textField: UITextField!) {
        
    }

    private func textFieldShouldEndEditing(textField: UITextField!) -> Bool {  //delegate method
        return false
    }

    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}
