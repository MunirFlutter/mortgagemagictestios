//
//  AuthVC.swift
//  MortgageApp
//
//  Created by rydus on 2/6/21.
//  Copyright © 2021 rydus. All rights reserved.
//  PageManager

import UIKit

class AuthVC: BaseVC, UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIScrollViewDelegate  {
    private var pageController: UIPageViewController!
    private var arrVC:[UIViewController] = []
    private var currentPage: Int!
    var extraSpace = CGFloat()
    // MARK: Login ViewController
    lazy var vc1: LoginVC = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        return viewController
    }()
    
    // MARK: Reg ViewController
    lazy var vc2: RegVC = {
        var viewController = storyboard?.instantiateViewController(withIdentifier: "RegVC") as! RegVC
        return viewController
    }()
    
    /*override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
        segmentedControl.selectedSegmentIndex = 1
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        title = "Join Mortgage Magic"
        setupPageMenu()
    }
    
    func setupPageMenu() {
        //Create controllers
        currentPage = 0
        extraSpace = screenSize.height*0.1
        ssss()
        createPageViewController()
        arrVC.append(vc1)
        arrVC.append(vc2)
    }
    
    var segmentedControl: CustomSegmentedContrl!
        
    func ssss() {
        segmentedControl = CustomSegmentedContrl.init(frame: CGRect.init(x: 0, y: extraSpace, width: self.view.frame.width, height: 45))
//        segmentedControl
        segmentedControl.backgroundColor = .link
        segmentedControl.commaSeperatedButtonTitles = "LOG IN, CREATE AN ACCOUNT"
        segmentedControl.addTarget(self, action: #selector(onChangeOfSegment(_:)), for: .valueChanged)
        self.view.addSubview(segmentedControl)
    }
        
        
    //MARK: - CreatePagination
    private func createPageViewController() {
        pageController = UIPageViewController.init(transitionStyle: UIPageViewController.TransitionStyle.scroll, navigationOrientation: UIPageViewController.NavigationOrientation.horizontal, options: nil)
        //pageController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pageController.view.backgroundColor = UIColor.clear
        pageController.delegate = self
        pageController.dataSource = self
        //pageController.view.translatesAutoresizingMaskIntoConstraints = true
        //pageController.view.setNeedsUpdateConstraints()
        
        for svScroll in pageController.view.subviews as! [UIScrollView] {
            svScroll.delegate = self
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.pageController.view.frame = CGRect(x: 0, y: self.segmentedControl.frame.maxY, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            
        }
        
       // arrVC = [vc1, vc2, vc3]
        
        pageController.setViewControllers([vc1], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        
        pageController.view.frame = CGRect(x: 0, y: -extraSpace, width: pageController.view.frame.size.width, height: pageController.view.frame.size.height)
        self.addChild(pageController)
        self.view.addSubview(pageController.view)
        pageController.didMove(toParent: self)
    }
    
    
    private func indexofviewController(viewCOntroller: UIViewController) -> Int {
        if(arrVC .contains(viewCOntroller)) {
            return arrVC.firstIndex(of: viewCOntroller)!
        }
        return -1
    }
    
    
    //MARK: - Pagination Delegate Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index - 1
        }
        
        if(index < 0) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = indexofviewController(viewCOntroller: viewController)
        
        if(index != -1) {
            index = index + 1
        }
        
        if(index >= arrVC.count) {
            return nil
        }
        else {
            return arrVC[index]
        }
        
    }
    
    func pageViewController(_ pageViewController1: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        if(completed) {
            currentPage = arrVC.index(of: (pageViewController1.viewControllers?.last)!)
            self.segmentedControl.selectedSegmentIndex = currentPage
            
            self.segmentedControl.updateSegmentedControlSegs(index: currentPage)
            
        }

    }
    
    
    /*
    
    private func addViewControllerAsChildViewController(childViewController: UIViewController) {
        
        addChildViewController(childViewController)
        
        view.addSubview(childViewController.view)
        childViewController.view.frame = CGRect.init(x: 0, y: 120, width: self.view.frame.width, height: self.view.frame.height)
        
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        childViewController.didMove(toParentViewController: self)
        
    }
    
    private func removeViewControllerAsChildViewController(childViewController: UIViewController) {
        
        childViewController.willMove(toParentViewController: nil)
        childViewController.view.removeFromSuperview()
        
        childViewController.removeFromParentViewController()
        
    }
    
    */

    @objc func onChangeOfSegment(_ sender: CustomSegmentedContrl) {
        switch sender.selectedSegmentIndex {
        case 0:
            pageController.setViewControllers([arrVC[0]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
            currentPage = 0
            
        case 1:
            if currentPage > 1{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
                currentPage = 1
            }else{
                pageController.setViewControllers([arrVC[1]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                currentPage = 1
                
            }
        
        default:
            break
        }
        
    }
        
}
