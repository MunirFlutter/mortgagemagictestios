//
//  RegVC.swift
//  MortgageApp
//
//  Created by rydus on 2/6/21.
//  Copyright © 2021 rydus. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import LabelSwitch

class RegVC: BaseVC {

    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var tfFNameView:UIView!
    @IBOutlet weak var tfLNameView:UIView!
    @IBOutlet weak var tfEmailView:UIView!
    @IBOutlet weak var tfPhoneView:UIView!
    @IBOutlet weak var tfPwdView:UIView!
    @IBOutlet weak var tfComNameView:UIView!
    @IBOutlet weak var fName:SkyFloatingLabelTextField!
    @IBOutlet weak var lName:SkyFloatingLabelTextField!
    @IBOutlet weak var email:SkyFloatingLabelTextField!
    @IBOutlet weak var phone:SkyFloatingLabelTextField!
    @IBOutlet weak var pwd:SkyFloatingLabelTextField!
    @IBOutlet weak var companyNameView:UIView!
    @IBOutlet weak var companyName:SkyFloatingLabelTextField!
    @IBOutlet weak var sw:UISwitch!
    @IBOutlet weak var terms:UILabel!
    @IBOutlet weak var btnFBLogin:UIButton!
    @IBOutlet weak var btnGLogin:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        upgradeUI()
       
    }
    
    func upgradeUI() {

        setupScrollviewConstraints(view: self.view, scrollView: scrollView)
        
        //  tmp
        email.text = "anisur.meh1001@yopmail.com"
        pwd.text = "1234"
        fName.text = "Ansiur"
        lName.text = "Rahman99"
        phone.text = "880175257440946"
        pwd.text = "1234"
        
        //  MARK:   TF delegate
        fName.delegate = self
        lName.delegate = self
        email.delegate = self
        phone.delegate = self
        pwd.delegate = self
        companyName.delegate = self
        fName.lineColor = .clear
        lName.lineColor = .clear
        email.lineColor = .clear
        phone.lineColor = .clear
        pwd.lineColor = .clear
        companyName.lineColor = .clear
        
        //  MARK:   view shade
        upgradeViewShade(tfFNameView)
        upgradeViewShade(tfLNameView)
        upgradeViewShade(tfEmailView)
        upgradeViewShade(tfPhoneView)
        upgradeViewShade(tfPwdView)
        upgradeViewShade(tfComNameView)
        
        //  MARK:   terms lable clickable into substring
        let plainAttributedString = NSMutableAttributedString(string: "By clicking on \"Log in\" you confirm that you accept the ", attributes: nil)
        let string = "Mortgage Magic Terms and Conditions"
        let attributedLinkString = NSMutableAttributedString(string: string, attributes:[NSAttributedString.Key.link: URL(string: "http://www.google.com")!])
        let fullAttributedString = NSMutableAttributedString()
        fullAttributedString.append(plainAttributedString)
        fullAttributedString.append(attributedLinkString)
        terms.isUserInteractionEnabled = true
        terms.attributedText = fullAttributedString
        
        //  MARK:   set fb icon
        btnFBLogin.setAttributedTextWithImagePrefix(image: #imageLiteral(resourceName: "fb_icon"), text: "Continue with Facebook", color: .white, for: .normal)

        //  MARK:   set google icon
        btnGLogin.setAttributedTextWithImagePrefix(image: #imageLiteral(resourceName: "g_icon"), text: "Sign in", color: .black, for: .normal)
        btnGLogin.backgroundColor = .white
        btnGLogin.addShadowView()
    }
    
    @IBAction func onSWChanged(_ sender : UISwitch!){
        if sender.isOn {
            companyNameView.isHidden =  false
        } else {
           companyNameView.isHidden =  true
        }
    }
    
    @IBAction func registerClicked(_ sender: UIButton) {
        //  MARK:   JSon API call
        if(fName.text?.count==0) {
            fName.becomeFirstResponder()
            showAlert(msg: "Please enter first name")
            return
        }
        else if(lName.text?.count==0) {
            lName.becomeFirstResponder()
            showAlert(msg: "Please enter last name")
            return
        }        
        else if(!ValidateMgr.isValidEmail(email.text!)) {
            email.becomeFirstResponder()
            showAlert(msg: "Please enter valid email")
            return
        }
        else if(phone.text?.count==0) {
            phone.becomeFirstResponder()
            showAlert(msg: "Please enter phone number")
            return
        }
        else if(pwd.text?.count==0) {
            pwd.becomeFirstResponder()
            showAlert(msg: "Please enter password")
            return
        }
        else if(sw.isOn && companyName.text?.count==0) {
            companyName.becomeFirstResponder()
            showAlert(msg: "Please enter company name")
            return
        }
        else {
            var comName = "";
            if(sw.isOn) {
                comName = companyName.text!
            }
            let param = [
                "Email": email.text!,
                "Password": pwd.text!,
                "FirstName": fName.text!,
                "LastName": lName.text!,
                "MobileNumber": phone.text!,
                "CompanyName":comName,
                "CommunityId": "1",
                "Persist": false,
                "CheckSignUpMobileNumber": false,
                "CountryCode": "880",
                "Status": "101",
                "OTPCode": "",
                "BirthDay": "01",
                "BirthMonth": "Mar",
                "BirthYear": "1983",
                "UserCompanyId": 1003,
                "dialCode": "880",
                "ConfirmPassword": pwd.text!,
                "confirmPassword": pwd.text!,
                "DateofBirth": "01-Mar-1983"
            ] as [String : Any]
        NetworkMgr.post(parameters: param, action_url:  Server.REG_URL, isCookie:true) { (dataResponse) in
                do {
                    guard let json = try JSONSerialization.jsonObject(with: dataResponse, options: []) as? NSDictionary else {
                        return
                    }
                    print(json.description)
                    //  MARK:   Dependency Injection:: injecting json into model
                    let model = try! JSONDecoder().decode(modelLogin.self, from: dataResponse)
                    if(!model.Success) {
                        let dic = model.ErrorMessages?.dictionaryObject
                        let arrayValues = dic?["register"] as! Array<Any>
                        DispatchQueue.main.async {
                            self.showAlert(msg: arrayValues[0] as! String)
                        }
                        return
                    }
                    else {
                        //  MARK:   on login success
                        let dic = model.ResponseData?.dictionaryObject
                        let dic2 = dic!["User"] as! Dictionary<String, Any>
                        let jsonData = try! JSONSerialization.data(withJSONObject: dic2, options: [])
                        self.setUserProfileDB(data: jsonData)
                        //  let decoded = String(data: jsonData!, encoding: .utf8)!
                        DispatchQueue.main.async {
                            UIApplication.shared.windows.first?.rootViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabDashboard") as! UITabBarController
                                       UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }
                }catch {
                    Common.Log(str: error.localizedDescription)
                }
            }
        }
    }
    
    @IBAction func loginFBClicked(_ sender: UIButton) {
           //   MARK:   Firebase Facebook enable
        self.showAlert(msg: "Need firebase facebook id, secret code auth implementation")
    }
    
    @IBAction func loginGClicked(_ sender: UIButton) {
            //  MARK:   Firebase Google GMail enable
        self.showAlert(msg: "Need firebase google gmail auth implementation")
    }
    
}

extension RegVC:UITextFieldDelegate {
    
    private func textFieldDidBeginEditing(textField: UITextField!) {
        
    }

    private func textFieldShouldEndEditing(textField: UITextField!) -> Bool {  //delegate method
        return false
    }

    private func textFieldShouldReturn(textField: UITextField!) -> Bool {   //delegate method
        textField.resignFirstResponder()
        return true
    }
}

