//
//  Server.swift
//  SCuty
//
//  Created by Rydus on 27/06/2019.
//  Copyright © 2019 Rydus. All rights reserved.
//

import Foundation
// MARK:    To define server url and api key stuff here...
struct Server {
    static let BASE_URL = "http://app.mortgage-magic.co.uk"
    static let LOGIN_URL = "/api/authentication/login"
    static let REG_URL = "/api/authentication/register"
    static let TASKLIST_URL = "/api/task/taskinformationbysearch/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#USER_ID#&Page=0&Count=30&Status=901"
    static let POSTCASE_URL = "/api/task/post"
}
